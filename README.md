# docker-compose:
_Образы для этого проекта основаны на образе python:3.6.15-slim и включают только зависимости указанные в названии.
Посмотреть образы можно в моем [gitlab container registry](https://gitlab.com/Max.JK/mytornadotest/container_registry)_

<details><summary>Структура</summary>

- alembic
  - Dockerfile
  - upgrade.sh
- web
  - Dockerfile
  - pull.sh
- .env
- docker-compose.yml
</details>

<details><summary>docker-compose - Детали</summary>

# upgrade.sh, pull.sh & .env

<details><summary>alembic/upgrade.sh</summary>

    until alembic upgrade head; do         until используется для первого запуска
      >&2 echo "DB is unavailable - retry" если контейнер alembic запустился раньше БД
      sleep 1

</details>

<details><summary>web/pull.sh</summary>
    
    cd tornadoweb/
    git init
    git remote add origin https://gitlab.com/Max.JK/mytornadotest.git
    git pull origin master Каждый запуск будет стартовать приложение из ветки master
</details>

<details><summary>.env</summary>

    DB=postgresql Какая БД используется
    DB_DRIVER=psycopg2 Драйвер для работы с базой
    DB_USER=pgsadmin Учетная запись БД 
    DB_PASSWORD=mypgsdbpasswd Пароль учтеной записи БД
    DB_ADDRESS=pgs Название для контейнера БД
    DB_NAME=pgsdb Имя БД
</details>


# volumes

<details><summary>docker-compose - volumes</summary>

    volumes:
        shared: # Используется для общего доступа к файлам между alembic и tornadoweb

    services:
        tornadoweb:
            volumes:
                -   shared:/home/tornadoweb <--
        alembic:
            volumes:
                -   shared:/home/tornadoweb <--
</details>

</details>

# Tornado

<details><summary>Структура Проекта</summary>

- `db`
  - `db_manager.py`Менеджер подключений к базе
  - `mypgsdb.py` Декларирование таблиц
- `handlers`
  - `base.py` Базовый хэндлер
  - `into.py` Хэндлер для получения получения и возвращения аргументов
- `migrations`
  - `versions`
    - `default.py` Стандартная версия таблицы, необходимая для приложения
  - `env.py` Конфигурационный файл alembic
  - `script.py.mako` Стандартный шаблок миграций alembic
- `results`
  - `result.py` Обработчик ошибок
  - `resultlist.py` Содержит список ошибок
- `utils`
  - `checker.py` Выполняет проверку аргумента на: количество ключей и int
  - `randgen.py` Утилита, которая генерирует cookie
- `alembic.ini` Настройки alembic
- `app.py` Основной файл tornado
- `settings.py` Задает настройки cookie
- `urls.py` Создает ссылку на страницу и ее хэндлер

</details>

<details><summary>Как запустить</summary>

Клонируйте проект или загрузите только файлы для docker-compose и запустите его с помощью команды:

`docker-compose up`

_OR_

_**Для этого вам нужнен установленный python3.6**_

Скопируйте и вставьте в терминал следующую команду:

    docker run --rm --name pgs -e POSTGRES_USER=pgsadmin -e POSTGRES_PASSWORD=mypgsdbpasswd -e POSTGRES_DB=pgsdb -p 5432:5432 -d postgres:14.0 && \
    cd to home/$USER/ && git clone https://gitlab.com/Max.JK/mytornadotest.git && \
    cd mytornadoweb/ && alembic upgrade head && python3.6 app.py
Для такого запуска каждый раз нужно будет делать `alembic upgrade head` в директории проекта, когда БД была создана (_в данном случае после остановки контейнера он удалится из-за флага_ `--rm`)

</details>

<details><summary>Как отправить данные</summary>

Чтобы отправить данные можно воспользоваться Postman или терминала с помощью curl;

**_curl:_**

    curl -d "key=array" -X POST 127.0.0.1:8888
    curl -d "key=record_id*" -X POST 127.0.0.1:8888

record_id - номер записи в БД, будет возвращен сервером после совершения POST запроса

**_POSTMAN:_**

Укажите адрес _сервер:порт_,
прим. `127.0.0.1:8888`

Откройте Body и выберите `form-data` или `x-www-form-urlencoded`

И добавьте ключ и его значение

KEY:`key` VALUE:`value`

для POST это будет массив, для GET это будет номер записи

</details>

<details><summary>Детали</summary>

В файлах `db/db_manager.py` и `migrations/env.py`
для запуска вне контейнера есть конструкция в начале:

<details><summary>db/db_manager.py:</summary>

    if not os.path.exists("/home/tornadoweb"):
        os.environ['DB'] = 'postgresql'
        os.environ['DB_DRIVER'] = 'psycopg2'
        os.environ['DB_USER'] = 'pgsadmin'
        os.environ['DB_PASSWORD'] = 'mypgsdbpasswd'
        os.environ['DB_ADDRESS'] = 'localhost'
        os.environ['DB_NAME'] = 'pgsdb'
</details>

<details><summary>migrations/env.py:</summary>

    if not os.path.exists("/home/tornadoweb"):
        os.environ['DB'] = 'postgresql'
        os.environ['DB_USER'] = 'pgsadmin'
        os.environ['DB_PASSWORD'] = 'mypgsdbpasswd'
        os.environ['DB_ADDRESS'] = 'localhost'
        os.environ['DB_NAME'] = 'pgsdb'
</details>

Они определяют переменные среды, их описание в docker compose - Детали - .env

</details>