import tornado.web
import datetime
import time
import asyncio

from tornado import gen

from db.mypgsdb import Base, Info
from results.result import myExcept
from results.resultlist import Results
from handlers.base import BaseHandler
from db.db_manager import DBManager


class HandlerDB(BaseHandler):
    def initialize(self):
        self.loop = asyncio.get_event_loop()

    async def get(self):
        self.task = self.loop.create_task(DBManager(self.request.arguments).return_item())
        await self.task
        self.write(self.task.result())

    async def post(self):
        self.task = self.loop.create_task(DBManager(self.request.arguments).insert_item())
        await self.task
        self.write(self.task.result())
