#!/bin/bash

set -e

cd /home/tornadoweb

until alembic upgrade head; do
  >&2 echo "DB is unavailable - retry"
  sleep 1
done
