Results = {
    0: {
        "status_code": 0,
        "description": "OK"
    },
    1: {
        "status_code": 1,
        "description": "To many keys"
    },
    2: {
        "status_code": 2,
        "description": "Key empty"
    },
    3: {
        "status_code": 3,
        "description": "Value not digital"
    },
    4: {
        "status_code": 4,
        "description": "Data with this id not found"
    },
    5: {
        "status_code": 5,
        "description": "DB not found"
    }
}
