from utils.randgen import gen, cookie


settings = {'cookie_secret': gen(32, cookie),
            'csrf_cookies': False
            }
