from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Info(Base):
    __tablename__ = 'info'
    id_record = Column(Integer, primary_key=True)
    datas = Column(String, nullable=False)
    sorted_datas = Column(String, nullable=False)
    date_record = Column(String, nullable=False)
