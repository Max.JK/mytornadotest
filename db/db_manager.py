import datetime
from db.mypgsdb import Info
from utils.checker import CheckItem
from results.result import myExcept
from results.resultlist import Results

import os.path
import asyncio

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

if not os.path.exists("/home/tornadoweb"):
    os.environ['DB'] = 'postgresql'
    os.environ['DB_DRIVER'] = 'psycopg2'
    os.environ['DB_USER'] = 'pgsadmin'
    os.environ['DB_PASSWORD'] = 'mypgsdbpasswd'
    os.environ['DB_ADDRESS'] = 'localhost'
    os.environ['DB_NAME'] = 'pgsdb'

credentials = f"{os.environ.get('DB')}+{os.environ.get('DB_DRIVER')}://{os.environ.get('DB_USER')}:{os.environ.get('DB_PASSWORD')}@{os.environ.get('DB_ADDRESS')}/{os.environ.get('DB_NAME')}"
engine = create_engine(credentials)
session = sessionmaker(bind=engine)


class DBManager(object):
    def __init__(self, item):
        self.session = session()
        self.item = item
        self.date = None
        self.query = None
        self.response = {}

    async def insert_item(self):
        try:
            self.date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.item = CheckItem(self.item, "post").check()
            self.item = Info(
                datas=str(self.item),
                sorted_datas=str(sorted(self.item)),
                date_record=str(self.date)
            )
            self.session.add(self.item)
            self.session.commit()
            self.response['Status'] = Results[0]['description']
            self.response['id'] = self.item.id_record

        except myExcept as mE:
            self.response = Results[mE.id]

        finally:
            return self.response

    async def return_item(self):
        try:
            self.item = CheckItem(self.item, "get").check()
            self.query = self.session.query(Info).filter(Info.id_record == self.item).first()
            if not self.query:
                raise myExcept(4)
            self.response['id'] = self.query.id_record
            self.response['array'] = self.query.datas
            self.response['result_array'] = self.query.sorted_datas
            self.response['date_of_creation'] = self.query.date_record

        except myExcept as mE:
            self.response = Results[mE.id]

        finally:
            return self.response
