import tornado.ioloop
import tornado.web
import tornado.httpserver
from tornado.options import options

from settings import *
from urls import url_patterns


class TornadoBoilerplate(tornado.web.Application):
    def __init__(self):
        print(settings)
        tornado.web.Application.__init__(self, url_patterns, **settings)


def main():
    app = TornadoBoilerplate()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
