from results.result import myExcept
from results.resultlist import Results


class CheckItem(object):
    def __init__(self, item, method):
        self.item = item
        self.method = method
        self.result = None

    def digit(self, item):
        if not item.isdigit():
            raise myExcept(3)

    def check(self):
        if len(self.item.keys()) > 1:
            raise myExcept(1)
        elif len(self.item.keys()) < 1:
            raise myExcept(2)
        else:
            self.result = self.item.popitem()[1][0].decode('utf-8')
            if self.method == "get":
                self.digit(self.result)
            elif self.method == "post":
                self.result = self.result.replace(" ", "").split(',')
                for arg in self.result:
                    self.digit(arg)
            return self.result
