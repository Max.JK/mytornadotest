import random
num = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
chars_uppercase = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", )
chars_lovercase = ("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z")
sym = ("!", "$", "%", "^", "&", "*", "(", ")", "_", "+", "-", "=", ".", ",", ":", "[", "]", "{", "}")

cookie = ()
cookie = chars_uppercase + chars_lovercase + num


def gen(x, x_sym):
    _val = ''
    for i in range(x):
        _val += random.choice(x_sym)
    return _val
