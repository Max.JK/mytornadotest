#!/bin/bash

set -e

cd tornadoweb/

if [ -f README.md ]; then
  echo "already exists"
  git fetch
  if git diff --quiet master origin/master; then
    echo "using last version"
  else
    echo "taking update"
    git pull origin master
    echo "pull ended"
  fi
else
  git init
  git remote add origin https://gitlab.com/Max.JK/mytornadotest.git
  git pull origin master
  echo "first pull ended"
fi
echo "starting app"
python app.py